import configparser
from orchestrator_modules.constants import OrchestratorConstants

# this class inherit from dict
class OrchestratorConfig(dict):
  prop_folder = OrchestratorConstants.prop_folder
  config = configparser.ConfigParser()
  config.read(f"{prop_folder}/orchestrator.properties")

  def __getitem__(self, item):
    return self.config[item]

oconfig = OrchestratorConfig()
ologconfig = oconfig['log']
oglobalconfig = oconfig['orchestrator']
