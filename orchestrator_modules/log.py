import logging
from orchestrator_modules.constants import OrchestratorConstants
from orchestrator_modules.config import ologconfig

class OrchestratorLogger:
  # create file handler which logs
  logsdir = OrchestratorConstants.logsdir
  fh = logging.FileHandler(f"{logsdir}/orchestrator.log")
  fh.setLevel(logging._nameToLevel[ologconfig['LogFileLevel']])
  # error file handler
  feh = logging.FileHandler(f"{logsdir}/orchestrator-error.log")
  feh.setLevel(logging.ERROR)
  # create console handler
  ch = logging.StreamHandler()
  ch.setLevel(logging._nameToLevel[ologconfig['LogConsoleLevel']])
  # create formatter and add it to the handlers
  formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  fh.setFormatter(formatter)
  feh.setFormatter(formatter)
  ch.setFormatter(formatter)

  INFO = logging.INFO
  DEBUG = logging.DEBUG
  ERROR = logging.ERROR
  FATAL = logging.FATAL
  WARN = logging.WARN
  WARNING = logging.WARNING

  def __init__(self, name):
    self.logger = logging.getLogger(name)
    self.logger.setLevel(self.DEBUG)
    # add the handlers to the logger
    self.logger.addHandler(self.fh)
    self.logger.addHandler(self.feh)
    self.logger.addHandler(self.ch)
  
  def log(self, message, level):
    self.logger.log(level, message)
  
  def info(self, message):
    self.logger.info(message)
  
  def debug(self, message):
    self.logger.debug(message)

  def warn(self, message):
    self.logger.warn(message)

  def warning(self, message):
    self.logger.warning(message)

  def error(self, message):
    self.logger.error(message)

  def exception(self, exception):
    self.logger.exception(exception)

  
  
 