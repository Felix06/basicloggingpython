import subprocess
from orchestrator_modules.log import OrchestratorLogger
from orchestrator_modules.config import oconfig

if __name__ == '__main__':
  ologger = OrchestratorLogger('orchestrator.py')
  result = subprocess.run(['test_orchestrator.bat'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  ologger.info(result.stdout.decode('utf-8').rstrip())
  if result.stderr != bytes():
    ologger.error(result.stderr.decode('utf-8').rstrip())
  else:
    ologger.info("No error")

  ologger.info("Info message")
  ologger.debug("Debug message")
  ologger.warn("Warn Message")
  ologger.warning("Warning message")
  ologger.error("Error message")

  ologger2 = OrchestratorLogger('ologger2')
  ologger2.info('testing second logger')

  logsection = oconfig['log']
  print(logsection)
  logfilelevel = oconfig['log']['LogFileLevel']
  print(logfilelevel)
